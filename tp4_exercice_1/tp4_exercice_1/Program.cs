﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp4_exercice_1
{
    public class Program
    {
        public static int CalculeDeLaireDunCarre(int cote)
        {
            return cote * cote;
        }
        public static int CalculeDeLaireDunRectangle(int longueur, int largeur)
        {
            return longueur * largeur;
        }
        public static int CalculeDeLaireDunTriangle(int BaseTriangle, int HauteurTriangle)
        {
            return (BaseTriangle * HauteurTriangle)/2;
        }
        public static bool ChercheSiLaPersonneEstMajeure(int AnneeRentre)
        {
            int AnneeActuel;
            AnneeActuel = DateTime.Now.Year;
            if ((AnneeActuel - AnneeRentre) >= 18)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static void Main(string[] args)
        {
        }
    }
}
