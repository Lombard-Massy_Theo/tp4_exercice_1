﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using tp4_exercice_1;
namespace tp4_exercice_1_test
{
    [TestClass]
    public class exercice_1Test
    {
        /*[TestMethod]
        public void CalculeDeLaireDunCarre1()
        {
            Assert.AreEqual(4, Program.CalculeDeLaireDunCarre(2));
        }
        [TestMethod]
        public void CalculeDeLaireDunCarre2()
        {
            Assert.AreEqual(16, Program.CalculeDeLaireDunCarre(4));
        }
        [TestMethod]
        public void CalculeDeLaireDunCarre3()
        {
            Assert.AreEqual(96, Program.CalculeDeLaireDunCarre(8));
        }
        [TestMethod]
        public void CalculeDeLaireDunCarre4()
        {
            Assert.AreEqual(12, Program.CalculeDeLaireDunCarre(7));
        }
        [TestMethod]
        public void CalculeDeLaireDunRectangle1()
        {
            Assert.AreEqual(4, Program.CalculeDeLaireDunRectangle(2, 2));
        }
        [TestMethod]
        public void CalculeDeLaireDunRectangle2()
        {
            Assert.AreEqual(28, Program.CalculeDeLaireDunRectangle(7, 4));
        }
        [TestMethod]
        public void CalculeDeLaireDunRectangle3()
        {
            Assert.AreEqual(96, Program.CalculeDeLaireDunRectangle(6, 8));
        }
        [TestMethod]
        public void CalculeDeLaireDunRectangle4()
        {
            Assert.AreEqual(14, Program.CalculeDeLaireDunRectangle(2, 7));
        }
        [TestMethod]
        public void CalculeDeLaireDunTriangle1()
        {
            Assert.AreEqual(8, Program.CalculeDeLaireDunTriangle(2, 2));
        }
        [TestMethod]
        public void CalculeDeLaireDunTriangle2()
        {
            Assert.AreEqual(14, Program.CalculeDeLaireDunTriangle(7, 4));
        }
        [TestMethod]
        public void CalculeDeLaireDunTriangle3()
        {
            Assert.AreEqual(96, Program.CalculeDeLaireDunTriangle(6, 8));
        }
        [TestMethod]
        public void CalculeDeLaireDunTriangle4()
        {
            Assert.AreEqual(7, Program.CalculeDeLaireDunTriangle(2, 7));
        }*/
        [TestMethod]
        public void CalculeDeLaDateDeNaissance1()
        {
            Assert.IsTrue(Program.ChercheSiLaPersonneEstMajeure(2018));
        }
        [TestMethod]
        public void CalculeDeLaDateDeNaissance2()
        {
            Assert.IsTrue(Program.ChercheSiLaPersonneEstMajeure(2000));
        }
        [TestMethod]
        public void CalculeDeLaDateDeNaissance3()
        {
            Assert.IsTrue(Program.ChercheSiLaPersonneEstMajeure(2016));
        }
        [TestMethod]
        public void CalculeDeLaDateDeNaissance4()
        {
            Assert.IsTrue(Program.ChercheSiLaPersonneEstMajeure(200));
        }
    }
}
